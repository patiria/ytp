# ytp

Searches and plays Youtube stuff.

You can make an API key here: https://console.developers.google.com/

Following [these](https://www.slickremix.com/docs/get-api-key-for-youtube/) instructions.

Then just replace it in line 10 of the script.

![](https://gitlab.com/uoou/ytp/raw/master/ss.png)

## Requirements

`jq`, `youtube-dl` and `mpv`.

## Installation

It's a bash script. Copy & paste/git clone/curl it, `chmod +x` it and put it in your path (after checking it doesn't do bad things (it doesn't do bad things)).

## Ting

GPL v. 2

By Drew.

I love you.

x
